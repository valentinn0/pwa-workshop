import { Component, OnInit, ViewChild } from '@angular/core';
import { TodosService } from './services/todos.service';
import { first } from 'rxjs/operators';
import { Todo } from './models/todo.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit{
  title = 'pwa-workshop';
  todos: Todo[];
  todo: Todo = new Todo();

  @ViewChild('f', {static: false}) todoForm: NgForm;

  constructor(private todosService: TodosService) { }

  ngOnInit() {
    this.getTodos();
  }

  createTodo(): void {
    this.todosService.createTodo(this.todo)
      .pipe(first())
      .subscribe(newTodo => {
        this.getTodos();
        this.todoForm.form.reset();
      });
  }

  completeTodo(todo: Todo): void {
    this.todosService.deleteTodo(todo)
      .pipe(first())
      .subscribe(() => {
        console.log('Todo was deleted!');
        this.getTodos();
      })
  }

  getTodos(): void {
    this.todosService.getTodos()
      .pipe(first())
      .subscribe(todos => {
        this.todos = todos;
      })
  }
}
